package com.neman.booknest.Service.ServiceImpl;

import com.neman.booknest.Dto.AuthorDto;
import com.neman.booknest.Mapper.AuthorMapper;
import com.neman.booknest.Model.Author;
import com.neman.booknest.Repository.AuthorRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class AuthorServiceImplTest {

    @Mock
    private AuthorRepository authorRepository;
    @Mock
    private AuthorMapper authorMapper;
    @InjectMocks
    private AuthorServiceImpl authorService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void createAuthor() {
        AuthorDto authorDto=new AuthorDto();
        Author author=new Author();
        when(authorMapper.toEntity(any(AuthorDto.class))).thenReturn(author);
        when(authorRepository.save(any(Author.class))).thenReturn(author);
        when(authorMapper.toDto(any(Author.class))).thenReturn(authorDto);

        AuthorDto result=authorService.createAuthor(authorDto);

        assertNotNull(result);
        verify(authorRepository,times(1)).save(any(Author.class));
    }

    @Test
    void getAuthorById() {
        Author author=new Author();
        AuthorDto authorDto=new AuthorDto();

        when(authorRepository.findById(anyLong())).thenReturn(Optional.of(author));
        when(authorMapper.toDto(any(Author.class))).thenReturn(authorDto);

        AuthorDto result=authorService.getAuthorById(1L);

        assertNotNull(result);
        verify(authorRepository, times(1)).findById(anyLong());
    }

    @Test
    void updateAuthor() {
        AuthorDto authorDto=new AuthorDto();
        Author author=new Author();

        when(authorRepository.findById(anyLong())).thenReturn(Optional.of(author));
        when(authorMapper.toEntity(any(AuthorDto.class))).thenReturn(author);
        when(authorRepository.save(any(Author.class))).thenReturn(author);
        when(authorMapper.toDto(any(Author.class))).thenReturn(authorDto);

        AuthorDto result=authorService.updateAuthor(1L,authorDto);

        assertNotNull(result);
        verify(authorRepository, times(1)).save(any(Author.class));
    }

    @Test
    void deleteAuthor() {
        Author author=new Author();
        author.setAuthorId(1L);

        when(authorRepository.findById(anyLong())).thenReturn(Optional.of(author));
        doNothing().when(authorRepository).delete(any(Author.class));

        authorService.deleteAuthor(1L);

        verify(authorRepository, times(1)).findById(anyLong());
        verify(authorRepository, times(1)).delete(any(Author.class));
    }

    @Test
    void findAllAuthors() {
        Author author1 = new Author();
        author1.setAuthorId(1L);
        author1.setAuthorName("Author 1");

        Author author2 = new Author();
        author2.setAuthorId(2L);
        author2.setAuthorName("Author 2");

        List<Author> authors = Arrays.asList(author1, author2);

        AuthorDto authorDto1 = new AuthorDto();
        authorDto1.setAuthorId(1L);
        authorDto1.setAuthorName("Author 1");

        AuthorDto authorDto2 = new AuthorDto();
        authorDto2.setAuthorId(2L);
        authorDto2.setAuthorName("Author 2");

        when(authorRepository.findAll()).thenReturn(authors);
        when(authorMapper.toDto(author1)).thenReturn(authorDto1);
        when(authorMapper.toDto(author2)).thenReturn(authorDto2);

        List<AuthorDto> result = authorService.findAllAuthors();

        assertEquals(2, result.size());
        assertEquals("Author 1", result.get(0).getAuthorName());
        assertEquals("Author 2", result.get(1).getAuthorName());

        verify(authorRepository, times(1)).findAll();
        verify(authorMapper, times(1)).toDto(author1);
        verify(authorMapper, times(1)).toDto(author2);
    }


    @Test
    void searchAuthorByAuthorName() {
        String authorName = "Author";

        Author author1 = new Author();
        author1.setAuthorId(1L);
        author1.setAuthorName("Author 1");

        Author author2 = new Author();
        author2.setAuthorId(2L);
        author2.setAuthorName("Author 2");

        List<Author> authors = Arrays.asList(author1, author2);

        AuthorDto authorDto1 = new AuthorDto();
        authorDto1.setAuthorId(1L);
        authorDto1.setAuthorName("Author 1");

        AuthorDto authorDto2 = new AuthorDto();
        authorDto2.setAuthorId(2L);
        authorDto2.setAuthorName("Author 2");

        when(authorRepository.findByAuthorNameContainingIgnoreCase(authorName)).thenReturn(authors);
        when(authorMapper.toDto(author1)).thenReturn(authorDto1);
        when(authorMapper.toDto(author2)).thenReturn(authorDto2);

        List<AuthorDto> result = authorService.searchAuthorByAuthorName(authorName);

        assertEquals(2, result.size());
        assertEquals("Author 1", result.get(0).getAuthorName());
        assertEquals("Author 2", result.get(1).getAuthorName());

        verify(authorRepository, times(1)).findByAuthorNameContainingIgnoreCase(authorName);
        verify(authorMapper, times(1)).toDto(author1);
        verify(authorMapper, times(1)).toDto(author2);
    }
}