package com.neman.booknest.Service.ServiceImpl;

import com.neman.booknest.Dto.PublisherDto;
import com.neman.booknest.Mapper.PublisherMapper;
import com.neman.booknest.Model.Author;
import com.neman.booknest.Model.Publisher;
import com.neman.booknest.Repository.PublisherRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class PublisherServiceImplTest {
    @Mock
    private PublisherRepository publisherRepository;

    @Mock
    private PublisherMapper publisherMapper;

    @InjectMocks
    private PublisherServiceImpl publisherService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void createPublisher() {
        PublisherDto publisherDto = new PublisherDto();
        Publisher publisher = new Publisher();

        when(publisherMapper.toEntity(any(PublisherDto.class))).thenReturn(publisher);
        when(publisherRepository.save(any(Publisher.class))).thenReturn(publisher);
        when(publisherMapper.toDto(any(Publisher.class))).thenReturn(publisherDto);

        PublisherDto result = publisherService.createPublisher(publisherDto);

        assertNotNull(result);
        verify(publisherRepository, times(1)).save(any(Publisher.class));
    }

    @Test
    void getPublisherById() {
        Publisher publisher = new Publisher();
        PublisherDto publisherDto = new PublisherDto();

        when(publisherRepository.findById(anyLong())).thenReturn(Optional.of(publisher));
        when(publisherMapper.toDto(any(Publisher.class))).thenReturn(publisherDto);

        PublisherDto result = publisherService.getPublisherById(1L);

        assertNotNull(result);
        verify(publisherRepository, times(1)).findById(anyLong());
    }

    @Test
    void updatePublisher() {
        PublisherDto publisherDto = new PublisherDto();
        Publisher publisher = new Publisher();

        when(publisherRepository.findById(anyLong())).thenReturn(Optional.of(publisher));
        when(publisherMapper.toEntity(any(PublisherDto.class))).thenReturn(publisher);
        when(publisherRepository.save(any(Publisher.class))).thenReturn(publisher);
        when(publisherMapper.toDto(any(Publisher.class))).thenReturn(publisherDto);

        PublisherDto result = publisherService.updatePublisher(1L, publisherDto);

        assertNotNull(result);
        verify(publisherRepository, times(1)).save(any(Publisher.class));
    }

    @Test
    void deletePublisher() {
        Publisher publisher=new Publisher();
        publisher.setId(1L);

        when(publisherRepository.findById(anyLong())).thenReturn(Optional.of(publisher));
        doNothing().when(publisherRepository).delete(any(Publisher.class));

        publisherService.deletePublisher(1L);

        verify(publisherRepository, times(1)).findById(anyLong());
        verify(publisherRepository, times(1)).delete(any(Publisher.class));
    }

    @Test
    void findAllPublisher() {
        Publisher publisher1 = new Publisher();
        publisher1.setId(1L);
        publisher1.setPublisherName("Test Publisher 1");

        Publisher publisher2 = new Publisher();
        publisher2.setId(2L);
        publisher2.setPublisherName("Test Publisher 2");

        List<Publisher> publishers = Arrays.asList(publisher1, publisher2);

        PublisherDto publisherDto1 = new PublisherDto();
        publisherDto1.setId(1L);
        publisherDto1.setPublisherName("Test Publisher 1");

        PublisherDto publisherDto2 = new PublisherDto();
        publisherDto2.setId(2L);
        publisherDto2.setPublisherName("Test Publisher 2");

        when(publisherRepository.findAll()).thenReturn(publishers);
        when(publisherMapper.toDto(publisher1)).thenReturn(publisherDto1);
        when(publisherMapper.toDto(publisher2)).thenReturn(publisherDto2);

        List<PublisherDto> result = publisherService.findAllPublisher();

        assertEquals(2, result.size());
        assertEquals("Test Publisher 1", result.get(0).getPublisherName());
        assertEquals("Test Publisher 2", result.get(1).getPublisherName());

        verify(publisherRepository, times(1)).findAll();
        verify(publisherMapper, times(1)).toDto(publisher1);
        verify(publisherMapper, times(1)).toDto(publisher2);
    }

    @Test
    void searchPublisherByName() {
        String searchName = "Test";

        Publisher publisher1 = new Publisher();
        publisher1.setId(1L);
        publisher1.setPublisherName("Test Publisher 1");

        Publisher publisher2 = new Publisher();
        publisher2.setId(2L);
        publisher2.setPublisherName("Another Test Publisher 2");

        List<Publisher> publishers = Arrays.asList(publisher1, publisher2);

        PublisherDto publisherDto1 = new PublisherDto();
        publisherDto1.setId(1L);
        publisherDto1.setPublisherName("Test Publisher 1");

        PublisherDto publisherDto2 = new PublisherDto();
        publisherDto2.setId(2L);
        publisherDto2.setPublisherName("Another Test Publisher 2");

        when(publisherRepository.findByPublisherNameContainingIgnoreCase(searchName)).thenReturn(publishers);
        when(publisherMapper.toDto(publisher1)).thenReturn(publisherDto1);
        when(publisherMapper.toDto(publisher2)).thenReturn(publisherDto2);

        List<PublisherDto> result = publisherService.searchPublisherByName(searchName);

        assertEquals(2, result.size());
        assertEquals("Test Publisher 1", result.get(0).getPublisherName());
        assertEquals("Another Test Publisher 2", result.get(1).getPublisherName());

        verify(publisherRepository, times(1)).findByPublisherNameContainingIgnoreCase(searchName);
        verify(publisherMapper, times(1)).toDto(publisher1);
        verify(publisherMapper, times(1)).toDto(publisher2);
    }
}