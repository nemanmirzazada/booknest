package com.neman.booknest.Service.ServiceImpl;

import com.neman.booknest.Dto.BookDto;
import com.neman.booknest.Exceptions.ResourceNotFoundException;
import com.neman.booknest.Mapper.BookMapper;
import com.neman.booknest.Model.Author;
import com.neman.booknest.Model.Book;
import com.neman.booknest.Model.Publisher;
import com.neman.booknest.Repository.AuthorRepository;
import com.neman.booknest.Repository.BookRepository;
import com.neman.booknest.Repository.PublisherRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class BookServiceImplTest {

    @Mock
    private BookRepository bookRepository;
    @Mock
    private AuthorRepository authorRepository;
    @Mock
    private PublisherRepository publisherRepository;
    @Mock
    private BookMapper bookMapper;
    @InjectMocks
    private BookServiceImpl bookService;

    private BookDto bookDto;
    private Book book;
    private Author author;
    private Publisher publisher;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        bookDto=new BookDto();
        bookDto.setBookName("Test book");
        bookDto.setBookPrice(new BigDecimal(100));
        bookDto.setLanguage("English");
        bookDto.setPage(200);
        bookDto.setISBN("dfd4555");
        bookDto.setAuthorIds(new HashSet<>(Arrays.asList(1L)));
        bookDto.setPublisherId(1L);

        book=new Book();
        book.setId(1L);
        book.setBookName("test book");
        book.setBookPrice(new BigDecimal(100));
        book.setLanguage("English");
        book.setPage(200);
        book.setISBN("4d4fdfs5");

        author=new Author();
        author.setAuthorId(1L);
        author.setAuthorName("Test author");

        publisher=new Publisher();
        publisher.setId(1L);
        publisher.setPublisherName("Test publisher");
    }

    @Test
    void getAllBooks() {
        // Arrange
        Book book1 = new Book();
        book1.setId(1L);
        book1.setBookName("Book 1");
        book1.setAuthors(Set.of(new Author(1L, "Author 1", Set.of(book1))));
        book1.setPublisher(new Publisher(1L, "Publisher 1", Set.of(book1)));

        Book book2 = new Book();
        book2.setId(2L);
        book2.setBookName("Book 2");
        book2.setAuthors(Set.of(new Author(2L, "Author 2", Set.of(book2))));
        book2.setPublisher(new Publisher(2L, "Publisher 2", Set.of(book2)));

        List<Book> books = List.of(book1, book2);

        BookDto bookDto1 = new BookDto();
        bookDto1.setId(1L);
        bookDto1.setBookName("Book 1");

        BookDto bookDto2 = new BookDto();
        bookDto2.setId(2L);
        bookDto2.setBookName("Book 2");

        when(bookRepository.findAllWithAuthorsAndPublisher()).thenReturn(books);
        when(bookMapper.toDto(book1)).thenReturn(bookDto1);
        when(bookMapper.toDto(book2)).thenReturn(bookDto2);

        // Act
        List<BookDto> result = bookService.getAllBooks();

        // Assert
        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals(bookDto1, result.get(0));
        assertEquals(bookDto2, result.get(1));

        verify(bookRepository, times(1)).findAllWithAuthorsAndPublisher();
        verify(bookMapper, times(1)).toDto(book1);
        verify(bookMapper, times(1)).toDto(book2);
    }

    @Test
    void testCreateBook_Success() {
        // Arrange
        BookDto bookDto = new BookDto();
        bookDto.setAuthorIds(Set.of(1L, 2L));
        bookDto.setPublisherId(1L);

        Author author1 = new Author();
        author1.setAuthorId(1L);
        Author author2 = new Author();
        author2.setAuthorId(2L);

        Publisher publisher = new Publisher();
        publisher.setId(1L);

        Book book = new Book();
        Book savedBook = new Book();
        BookDto savedBookDto = new BookDto();

        when(authorRepository.findById(1L)).thenReturn(Optional.of(author1));
        when(authorRepository.findById(2L)).thenReturn(Optional.of(author2));
        when(publisherRepository.findById(1L)).thenReturn(Optional.of(publisher));
        when(bookMapper.toEntity(bookDto)).thenReturn(book);
        when(bookRepository.save(book)).thenReturn(savedBook);
        when(bookMapper.toDto(savedBook)).thenReturn(savedBookDto);

        // Act
        BookDto result = bookService.createBook(bookDto);

        // Assert
        assertNotNull(result);
        verify(authorRepository, times(1)).findById(1L);
        verify(authorRepository, times(1)).findById(2L);
        verify(publisherRepository, times(1)).findById(1L);
        verify(bookRepository, times(1)).save(book);
        verify(bookMapper, times(1)).toEntity(bookDto);
        verify(bookMapper, times(1)).toDto(savedBook);
    }

    @Test
    void testCreateBook_AuthorNotFound() {
        // Arrange
        BookDto bookDto = new BookDto();
        bookDto.setAuthorIds(Set.of(1L));
        bookDto.setPublisherId(1L);

        when(authorRepository.findById(1L)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> bookService.createBook(bookDto));

        verify(authorRepository, times(1)).findById(1L);
        verify(publisherRepository, never()).findById(anyLong());
        verify(bookRepository, never()).save(any(Book.class));
        verify(bookMapper, never()).toEntity(any(BookDto.class));
        verify(bookMapper, never()).toDto(any(Book.class));
    }

    @Test
    void testCreateBook_PublisherNotFound() {
        // Arrange
        BookDto bookDto = new BookDto();
        bookDto.setAuthorIds(Set.of(1L));
        bookDto.setPublisherId(1L);

        Author author = new Author();
        author.setAuthorId(1L);

        when(authorRepository.findById(1L)).thenReturn(Optional.of(author));
        when(publisherRepository.findById(1L)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> bookService.createBook(bookDto));

        verify(authorRepository, times(1)).findById(1L);
        verify(publisherRepository, times(1)).findById(1L);
        verify(bookRepository, never()).save(any(Book.class));
        verify(bookMapper, never()).toEntity(any(BookDto.class));
        verify(bookMapper, never()).toDto(any(Book.class));
    }

    @Test
    void testCreateBook_UnexpectedError() {
        // Arrange
        BookDto bookDto = new BookDto();
        bookDto.setAuthorIds(Set.of(1L));
        bookDto.setPublisherId(1L);

        Author author = new Author();
        author.setAuthorId(1L);

        when(authorRepository.findById(1L)).thenReturn(Optional.of(author));
        when(publisherRepository.findById(1L)).thenThrow(new RuntimeException("Unexpected error"));

        // Act & Assert
        RuntimeException exception = assertThrows(RuntimeException.class, () -> bookService.createBook(bookDto));
        assertEquals("Unexpected error", exception.getMessage());

        verify(authorRepository, times(1)).findById(1L);
        verify(publisherRepository, times(1)).findById(1L);
        verify(bookRepository, never()).save(any(Book.class));
        verify(bookMapper, never()).toEntity(any(BookDto.class));
        verify(bookMapper, never()).toDto(any(Book.class));
    }

    @Test
    void creatBook_PublisherNotFound(){
        when(authorRepository.findById(1L)).thenReturn(Optional.of(author));
        when(publisherRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, ()->{
            bookService.createBook(bookDto);
                });

        verify(authorRepository, times(1)).findById(1L);
        verify(publisherRepository, times(1)).findById(1L);
        verify(bookRepository, never()).save(any(Book.class));

    }


    @Test
    void deleteBook() {
        when(bookRepository.findById(anyLong())).thenReturn(Optional.of(book));
        doNothing().when(bookRepository).delete(any(Book.class));

        bookService.deleteBook(1L);

        verify(bookRepository, times(1)).findById(anyLong());
        verify(bookRepository, times(1)).delete(any(Book.class));
    }

    @Test
    void updateBook() {
        when(bookRepository.findById(anyLong())).thenReturn(Optional.of(book));
        when(authorRepository.findById(anyLong())).thenReturn(Optional.of(author));
        when(publisherRepository.findById(anyLong())).thenReturn(Optional.of(publisher));
        when(bookMapper.toEntity(any(BookDto.class))).thenReturn(book);
        when(bookRepository.save(any(Book.class))).thenReturn(book);
        when(bookMapper.toDto(any(Book.class))).thenReturn(bookDto);

        BookDto result = bookService.updateBook(1L, bookDto);

        assertNotNull(result);
        verify(bookRepository, times(1)).save(any(Book.class));
    }

    @Test
    void getBookById() {
        when(bookRepository.findById(anyLong())).thenReturn(Optional.of(book));
        when(bookMapper.toDto(any(Book.class))).thenReturn(bookDto);

        BookDto result = bookService.getBookById(1L);

        assertNotNull(result);
        verify(bookRepository, times(1)).findById(anyLong());
    }

    @Test
    void findBooksByTitle() {
        String title = "Test Title";

        Book book1 = new Book();
        book1.setId(1L);
        book1.setBookName("Test Title 1");

        Book book2 = new Book();
        book2.setId(2L);
        book2.setBookName("Test Title 2");

        List<Book> books = Arrays.asList(book1, book2);

        BookDto bookDto1 = new BookDto();
        bookDto1.setId(1L);
        bookDto1.setBookName("Test Title 1");

        BookDto bookDto2 = new BookDto();
        bookDto2.setId(2L);
        bookDto2.setBookName("Test Title 2");

        when(bookRepository.findByTitleContaining(title)).thenReturn(books);
        when(bookMapper.toDto(book1)).thenReturn(bookDto1);
        when(bookMapper.toDto(book2)).thenReturn(bookDto2);

        List<BookDto> result = bookService.findBooksByTitle(title);

        assertEquals(2, result.size());
        assertEquals("Test Title 1", result.get(0).getBookName());
        assertEquals("Test Title 2", result.get(1).getBookName());

        verify(bookRepository, times(1)).findByTitleContaining(title);
        verify(bookMapper, times(1)).toDto(book1);
        verify(bookMapper, times(1)).toDto(book2);
    }

    @Test
    void findBooksByAuthorName() {
        String authorName = "Test Author";

        Book book1 = new Book();
        book1.setId(1L);
        book1.setBookName("Test Book 1");

        Book book2 = new Book();
        book2.setId(2L);
        book2.setBookName("Test Book 2");

        List<Book> books = Arrays.asList(book1, book2);

        BookDto bookDto1 = new BookDto();
        bookDto1.setId(1L);
        bookDto1.setBookName("Test Book 1");

        BookDto bookDto2 = new BookDto();
        bookDto2.setId(2L);
        bookDto2.setBookName("Test Book 2");

        when(bookRepository.findByAuthorNameContaining(authorName)).thenReturn(books);
        when(bookMapper.toDto(book1)).thenReturn(bookDto1);
        when(bookMapper.toDto(book2)).thenReturn(bookDto2);

        List<BookDto> result = bookService.findBooksByAuthorName(authorName);

        assertEquals(2, result.size());
        assertEquals("Test Book 1", result.get(0).getBookName());
        assertEquals("Test Book 2", result.get(1).getBookName());

        verify(bookRepository, times(1)).findByAuthorNameContaining(authorName);
        verify(bookMapper, times(1)).toDto(book1);
        verify(bookMapper, times(1)).toDto(book2);
    }

    @Test
    void findBooksByPublisherName() {
        String publisherName = "Test Publisher";

        Book book1 = new Book();
        book1.setId(1L);
        book1.setBookName("Test Book 1");

        Book book2 = new Book();
        book2.setId(2L);
        book2.setBookName("Test Book 2");

        List<Book> books = Arrays.asList(book1, book2);

        BookDto bookDto1 = new BookDto();
        bookDto1.setId(1L);
        bookDto1.setBookName("Test Book 1");

        BookDto bookDto2 = new BookDto();
        bookDto2.setId(2L);
        bookDto2.setBookName("Test Book 2");

        when(bookRepository.findByPublisherNameContaining(publisherName)).thenReturn(books);
        when(bookMapper.toDto(book1)).thenReturn(bookDto1);
        when(bookMapper.toDto(book2)).thenReturn(bookDto2);

        List<BookDto> result = bookService.findBooksByPublisherName(publisherName);

        assertEquals(2, result.size());
        assertEquals("Test Book 1", result.get(0).getBookName());
        assertEquals("Test Book 2", result.get(1).getBookName());

        verify(bookRepository, times(1)).findByPublisherNameContaining(publisherName);
        verify(bookMapper, times(1)).toDto(book1);
        verify(bookMapper, times(1)).toDto(book2);
    }
}