package com.neman.booknest.Service.ServiceImpl;

import com.neman.booknest.Dto.UserDto;
import com.neman.booknest.Exceptions.ResourceAlreadyExistException;
import com.neman.booknest.Exceptions.ResourceNotFoundException;
import com.neman.booknest.Mapper.UserMapper;
import com.neman.booknest.Model.Role;
import com.neman.booknest.Model.User;
import com.neman.booknest.Repository.RoleRepository;
import com.neman.booknest.Repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class UserServiceImplTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private UserMapper userMapper;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserServiceImpl userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void registerUser() {
        UserDto userDto = new UserDto();
        userDto.setUsername("testuser");
        userDto.setPassword("password");
        User user = new User();
        Role role = new Role();
        role.setRoleName("USER");
        Set<Role> roles = new HashSet<>();
        roles.add(role);

        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        when(roleRepository.findByRoleName(anyString())).thenReturn(role);
        when(userRepository.save(any(User.class))).thenReturn(user);
        when(passwordEncoder.encode(anyString())).thenReturn("encodedPassword");
        when(userMapper.toDto(any(User.class))).thenReturn(userDto);

        UserDto result = userService.registerUser(userDto);

        assertNotNull(result);
        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    void testRegisterUserThrowsException() {
        UserDto userDto = new UserDto();
        userDto.setUsername("testuser");

        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(new User()));

        assertThrows(ResourceAlreadyExistException.class, () -> userService.registerUser(userDto));
        verify(userRepository, times(0)).save(any(User.class));
    }
    @Test
    void registerAdmin_WhenUsernameExists() {
        UserDto userDto = new UserDto();
        userDto.setUsername("admin");
        userDto.setPassword("password");

        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(new User()));

        assertThrows(ResourceAlreadyExistException.class, () -> userService.registerAdmin(userDto));

        verify(userRepository, times(1)).findByUsername(userDto.getUsername());
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    void registerAdmin_WhenUsernameDoesNotExist() {
        UserDto userDto = new UserDto();
        userDto.setUsername("admin");
        userDto.setPassword("password");

        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setPassword("encodedPassword");

        Role adminRole = new Role();
        adminRole.setRoleName("ADMIN");

        UserDto savedUserDto = new UserDto();
        savedUserDto.setUsername("admin");

        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        when(passwordEncoder.encode(anyString())).thenReturn("encodedPassword");
        when(roleRepository.findByRoleName("ADMIN")).thenReturn(adminRole);
        when(userRepository.save(any(User.class))).thenReturn(user);
        when(userMapper.toDto(any(User.class))).thenReturn(savedUserDto);

        UserDto result = userService.registerAdmin(userDto);

        assertNotNull(result);
        assertEquals("admin", result.getUsername());

        verify(userRepository, times(1)).findByUsername(userDto.getUsername());
        verify(passwordEncoder, times(1)).encode(userDto.getPassword());
        verify(roleRepository, times(1)).findByRoleName("ADMIN");
        verify(userRepository, times(1)).save(any(User.class));
        verify(userMapper, times(1)).toDto(any(User.class));
    }

    @Test
    void findByUsername_UserExists() {
        // Arrange
        String username = "admin";
        User user = new User();
        user.setUsername(username);
        UserDto userDto = new UserDto();
        userDto.setUsername(username);

        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));
        when(userMapper.toDto(any(User.class))).thenReturn(userDto);

        // Act
        UserDto result = userService.findByUsername(username);

        // Assert
        assertNotNull(result);
        assertEquals(username, result.getUsername());

        verify(userRepository, times(1)).findByUsername(username);
        verify(userMapper, times(1)).toDto(user);
    }

    @Test
    void findByUsername_UserDoesNotExist() {
        // Arrange
        String username = "admin";

        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(NoSuchElementException.class, () -> userService.findByUsername(username));

        verify(userRepository, times(1)).findByUsername(username);
        verify(userMapper, never()).toDto(any(User.class));
    }

    @Test
    void updateUser_UserExists() {
        // Arrange
        Long userId = 1L;
        User existingUser = new User();
        existingUser.setId(userId);
        existingUser.setUsername("oldUsername");

        UserDto userDto = new UserDto();
        userDto.setUsername("newUsername");
        userDto.setPassword("newPassword");
        Set<String> roleNames = new HashSet<>();
        roleNames.add("USER");
        userDto.setRoleNames(roleNames);

        Role userRole = new Role();
        userRole.setRoleName("USER");

        User updatedUser = new User();
        updatedUser.setId(userId);
        updatedUser.setUsername("newUsername");
        updatedUser.setPassword("encodedPassword");
        updatedUser.setRoles(Set.of(userRole));

        UserDto updatedUserDto = new UserDto();
        updatedUserDto.setUsername("newUsername");
        updatedUserDto.setRoleNames(roleNames);

        when(userRepository.findById(anyLong())).thenReturn(Optional.of(existingUser));
        when(passwordEncoder.encode(anyString())).thenReturn("encodedPassword");
        when(roleRepository.findByRoleName(anyString())).thenReturn(userRole);
        when(userRepository.save(any(User.class))).thenReturn(updatedUser);
        when(userMapper.toDto(any(User.class))).thenReturn(updatedUserDto);

        // Act
        UserDto result = userService.updateUser(userId, userDto);

        // Assert
        assertNotNull(result);
        assertEquals("newUsername", result.getUsername());
        assertTrue(result.getRoleNames().contains("USER"));

        verify(userRepository, times(1)).findById(userId);
        verify(passwordEncoder, times(1)).encode("newPassword");
        verify(roleRepository, times(1)).findByRoleName("USER");
        verify(userRepository, times(1)).save(any(User.class));
        verify(userMapper, times(1)).toDto(any(User.class));
    }

    @Test
    void testUpdateUser_UserNotFound() {
        // Arrange
        Long userId = 1L;
        UserDto userDto = new UserDto();

        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> userService.updateUser(userId, userDto));

        verify(userRepository, times(1)).findById(userId);
        verify(passwordEncoder, never()).encode(anyString());
        verify(roleRepository, never()).findByRoleName(anyString());
        verify(userRepository, never()).save(any(User.class));
        verify(userMapper, never()).toDto(any(User.class));
    }
}