package com.neman.booknest.Mapper;

import com.neman.booknest.Dto.RoleDto;
import com.neman.booknest.Model.Role;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface RoleMapper {
    RoleMapper INSTANCE = Mappers.getMapper(RoleMapper.class);

    RoleDto toDto(Role role);

    @Mapping(target = "id", ignore = true)
    Role toEntity(RoleDto roleDto);
}
