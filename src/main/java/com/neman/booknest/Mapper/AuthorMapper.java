package com.neman.booknest.Mapper;


import com.neman.booknest.Dto.AuthorDto;
import com.neman.booknest.Model.Author;
import com.neman.booknest.Model.Book;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface AuthorMapper {

    AuthorMapper INSTANCE = Mappers.getMapper(AuthorMapper.class);

    @Mapping(source = "books", target = "bookIds")
    AuthorDto toDto(Author author);

    @Mapping(source = "bookIds", target = "books")
    @Mapping(target = "authorId", ignore = true)
    Author toEntity(AuthorDto authorDto);

    default Set<Long> mapBooksToBookIds(Set<Book> books) {
        if (books == null) {
            return new HashSet<>();
        }
        return books.stream().map(Book::getId).collect(Collectors.toSet());
    }

    default Set<Book> mapBookIdsToBooks(Set<Long> bookIds) {
        if (bookIds == null) {
            return new HashSet<>();
        }
        return bookIds.stream().map(id -> {
            Book book = new Book();
            book.setId(id);
            return book;
        }).collect(Collectors.toSet());
    }



}
