package com.neman.booknest.Mapper;

import com.neman.booknest.Dto.BookDto;
import com.neman.booknest.Model.Author;
import com.neman.booknest.Model.Book;
import com.neman.booknest.Model.Publisher;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.Set;
import java.util.stream.Collectors;


@Mapper(componentModel = "spring", uses = {AuthorMapper.class, PublisherMapper.class})
public interface BookMapper {

    BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);

    @Mapping(source = "publisher.id", target = "publisherId")
    @Mapping(source = "authors", target = "authorIds")
    BookDto toDto(Book book);

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "publisherId", target = "publisher")
    @Mapping(source = "authorIds", target = "authors")
    Book toEntity(BookDto bookDto);

    default Long mapPublisherToId(Publisher publisher) {
        return publisher != null ? publisher.getId() : null;
    }

    default Publisher mapIdToPublisher(Long id) {
        Publisher publisher = new Publisher();
        publisher.setId(id);
        return publisher;
    }

    default Set<Long> mapAuthorsToIds(Set<Author> authors) {
        return authors.stream().map(Author::getAuthorId).collect(Collectors.toSet());
    }

    default Set<Author> mapIdsToAuthors(Set<Long> ids) {
        return ids.stream().map(id -> {
            Author author = new Author();
            author.setAuthorId(id);
            return author;
        }).collect(Collectors.toSet());
    }




}
