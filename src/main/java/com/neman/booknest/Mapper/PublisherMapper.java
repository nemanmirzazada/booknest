package com.neman.booknest.Mapper;

import com.neman.booknest.Dto.PublisherDto;
import com.neman.booknest.Model.Publisher;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PublisherMapper {

    PublisherMapper INSTANCE = Mappers.getMapper(PublisherMapper.class);

    PublisherDto toDto(Publisher publisher);

    @Mapping(target = "id", ignore = true)
    Publisher toEntity(PublisherDto publisherDto);


}
