package com.neman.booknest.Mapper;

import com.neman.booknest.Dto.UserDto;
import com.neman.booknest.Model.Role;
import com.neman.booknest.Model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {RoleMapper.class})
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(source = "roles", target = "roleNames")
    UserDto toDto(User user);

    @Mapping(source = "roleNames", target = "roles")
    User toEntity(UserDto userDto);

    default Set<String> mapRolesToNames(Set<Role> roles) {
        return roles.stream().map(Role::getRoleName).collect(Collectors.toSet());
    }

    default Set<Role> mapNamesToRoles(Set<String> names) {
        return names.stream().map(name -> {
            Role role = new Role();
            role.setRoleName(name);
            return role;
        }).collect(Collectors.toSet());
    }
}
