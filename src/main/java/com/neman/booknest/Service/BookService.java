package com.neman.booknest.Service;

import com.neman.booknest.Dto.BookDto;
import com.neman.booknest.Model.Book;

import java.util.List;

public interface BookService {
    List<BookDto> getAllBooks();
    BookDto createBook(BookDto bookDto);
    void deleteBook(Long id);
    BookDto updateBook(Long id,BookDto bookDto);
    BookDto getBookById(Long id);
    List<BookDto> findBooksByTitle(String title);
    List<BookDto> findBooksByAuthorName(String authorName);
    List<BookDto> findBooksByPublisherName(String publisherName);

}
