package com.neman.booknest.Service;

import com.neman.booknest.Dto.PublisherDto;

import java.util.List;

public interface PublisherService {
    PublisherDto createPublisher(PublisherDto publisherDto);
    PublisherDto getPublisherById(Long id);
    PublisherDto updatePublisher(Long id, PublisherDto publisherDto);
    void deletePublisher(Long id);
    List<PublisherDto> findAllPublisher();
    List<PublisherDto> searchPublisherByName(String publisherName);
}
