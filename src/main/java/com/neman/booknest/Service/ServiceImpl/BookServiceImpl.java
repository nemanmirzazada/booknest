package com.neman.booknest.Service.ServiceImpl;

import com.neman.booknest.Dto.BookDto;
import com.neman.booknest.Exceptions.ResourceNotFoundException;
import com.neman.booknest.Mapper.BookMapper;
import com.neman.booknest.Model.Author;
import com.neman.booknest.Model.Book;
import com.neman.booknest.Model.Publisher;
import com.neman.booknest.Repository.AuthorRepository;
import com.neman.booknest.Repository.BookRepository;
import com.neman.booknest.Repository.PublisherRepository;
import com.neman.booknest.Service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Slf4j
public class BookServiceImpl implements BookService {
    private static final Logger logger= LoggerFactory.getLogger(BookServiceImpl.class);
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final PublisherRepository publisherRepository;
    private final BookMapper bookMapper;

    @Transactional(readOnly = true)
    @Override
    public List<BookDto> getAllBooks() {
        List<Book>books=bookRepository.findAllWithAuthorsAndPublisher();
        return books.stream()
                .map(bookMapper::toDto)
                .collect(Collectors.toList());
    }


    @Transactional
    @Override
    public BookDto createBook(BookDto bookDto) {
        try {
            logger.debug("Creating book: {}",bookDto);
            Set<Author>authors=new HashSet<>();

            for (Long authorId:bookDto.getAuthorIds()){
                Author author=authorRepository.findById(authorId)
                        .orElseThrow(()-> new ResourceNotFoundException("Author not found with id:"+authorId));
                authors.add(author);
            }

            Publisher publisher=publisherRepository.findById(bookDto.getPublisherId())
                    .orElseThrow(()->new ResourceNotFoundException("Publisher not found with id:"+bookDto.getPublisherId()));

            Book book=bookMapper.toEntity(bookDto);
            book.setAuthors(authors);
            book.setPublisher(publisher);
            book=bookRepository.save(book);
            return bookMapper.toDto(book);
        }catch (ResourceNotFoundException e){
            logger.error("Resource not found: {}",e.getMessage(),e);
            throw e;
        }catch (Exception e){
            logger.error("Unexpected error occurred while creating book: {}", e.getMessage(), e);
            throw e;
        }
    }

    @Transactional
    @Override
    public void deleteBook(Long id) {
        Book book=bookRepository.findById(id)
                .orElseThrow(()->new ResourceNotFoundException("Book not found with id: "+id));
        bookRepository.delete(book);
    }

    @Transactional
    @Override
    public BookDto updateBook(Long id, BookDto bookDto) {
        Book existingBook=bookRepository.findById(id)
                .orElseThrow(()->new ResourceNotFoundException("Book not found with id: "+id));

        Set<Author>authors=new HashSet<>();
        for (Long authorId : bookDto.getAuthorIds()){
            Author author=authorRepository.findById(authorId)
                    .orElseThrow(()->new ResourceNotFoundException("Author not found with id: "+id));
            authors.add(author);
        }

        Publisher publisher=publisherRepository.findById(bookDto.getPublisherId())
                .orElseThrow(()->new ResourceNotFoundException("Publisher not found with id: "+id));

        existingBook.setBookName(bookDto.getBookName());
        existingBook.setBookPrice(bookDto.getBookPrice());
        existingBook.setLanguage(bookDto.getLanguage());
        existingBook.setPage(bookDto.getPage());
        existingBook.setISBN(bookDto.getISBN());
        existingBook.setAuthors(authors);
        existingBook.setPublisher(publisher);

        existingBook=bookRepository.save(existingBook);
        return bookMapper.toDto(existingBook);
    }

    @Transactional(readOnly = true)
    @Override
    public BookDto getBookById(Long id) {
        Book book=bookRepository.findById(id)
                .orElseThrow(()->new ResourceNotFoundException("Book not found with id: "+id));
        return bookMapper.toDto(book);
    }

    @Transactional(readOnly = true)
    @Override
    public List<BookDto> findBooksByTitle(String title) {
        List<Book>books=bookRepository.findByTitleContaining(title);
        return books.stream()
                .map(bookMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<BookDto> findBooksByAuthorName(String authorName) {
        List<Book>books=bookRepository.findByAuthorNameContaining(authorName);
        return books.stream()
                .map(bookMapper::toDto)
                .collect(Collectors.toList());

    }

    @Transactional(readOnly = true)
    @Override
    public List<BookDto> findBooksByPublisherName(String publisherName) {
        List<Book>books=bookRepository.findByPublisherNameContaining(publisherName);
        return books.stream()
                .map(bookMapper::toDto)
                .collect(Collectors.toList());
    }


}
