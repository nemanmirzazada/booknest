package com.neman.booknest.Service.ServiceImpl;

import com.neman.booknest.Dto.PublisherDto;
import com.neman.booknest.Exceptions.ResourceAlreadyExistException;
import com.neman.booknest.Exceptions.ResourceNotFoundException;
import com.neman.booknest.Mapper.PublisherMapper;
import com.neman.booknest.Model.Publisher;
import com.neman.booknest.Repository.PublisherRepository;
import com.neman.booknest.Service.PublisherService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class PublisherServiceImpl implements PublisherService {
    private final PublisherRepository publisherRepository;
    private final PublisherMapper publisherMapper;

    @Transactional
    @Override
    public PublisherDto createPublisher(PublisherDto publisherDto) {
        if (publisherRepository.existsByPublisherName(publisherDto.getPublisherName())){
            throw new ResourceAlreadyExistException("Publisher with this name already exists");
        }
        Publisher publisher=publisherMapper.toEntity(publisherDto);
        publisher=publisherRepository.save(publisher);
        return publisherMapper.toDto(publisher);
    }

    @Transactional(readOnly = true)
    @Override
    public PublisherDto getPublisherById(Long id) {
        Publisher publisher=publisherRepository.findById(id)
                .orElseThrow(()->new ResourceNotFoundException("Publisher not found with id: " + id));
        return publisherMapper.toDto(publisher);

    }

    @Transactional
    @Override
    public PublisherDto updatePublisher(Long id, PublisherDto publisherDto) {
        Publisher publisher=publisherRepository.findById(id)
                .orElseThrow(()->new ResourceNotFoundException("Publisher not found with id: " + id));
        publisher.setPublisherName(publisherDto.getPublisherName());
        publisher=publisherRepository.save(publisher);
        return publisherMapper.toDto(publisher);
    }

    @Transactional
    @Override
    public void deletePublisher(Long id) {
        Publisher publisher=publisherRepository.findById(id)
                .orElseThrow(()->new ResourceNotFoundException("Publisher not found with id: " + id));
        publisherRepository.delete(publisher);
    }

    @Transactional(readOnly = true)
    @Override
    public List<PublisherDto> findAllPublisher() {
        return publisherRepository.findAll().stream()
                .map(publisherMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<PublisherDto> searchPublisherByName(String publisherName) {
        return publisherRepository.findByPublisherNameContainingIgnoreCase(publisherName).stream()
                .map(publisherMapper::toDto)
                .collect(Collectors.toList());
    }
}
