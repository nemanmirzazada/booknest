package com.neman.booknest.Service.ServiceImpl;

import com.neman.booknest.Dto.AuthorDto;
import com.neman.booknest.Exceptions.ResourceAlreadyExistException;
import com.neman.booknest.Exceptions.ResourceNotFoundException;
import com.neman.booknest.Mapper.AuthorMapper;
import com.neman.booknest.Model.Author;
import com.neman.booknest.Repository.AuthorRepository;
import com.neman.booknest.Service.AuthorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthorServiceImpl implements AuthorService {
    private final AuthorRepository authorRepository;
    private final AuthorMapper authorMapper;

    @Transactional
    @Override
    public AuthorDto createAuthor(AuthorDto authorDto) {
        if (authorRepository.existsByAuthorName(authorDto.getAuthorName())){
            throw new ResourceAlreadyExistException("Author with this name already exists");
        }
        Author author=authorMapper.toEntity(authorDto);
        author=authorRepository.save(author);
        return authorMapper.toDto(author);

    }

    @Transactional(readOnly = true)
    @Override
    public AuthorDto getAuthorById(Long id) {
        Author author=authorRepository.findById(id)
                .orElseThrow(()->new ResourceNotFoundException("Author not found with id: " + id));
        return authorMapper.toDto(author);
    }

    @Transactional
    @Override
    public AuthorDto updateAuthor(Long id, AuthorDto authorDto) {
        Author author=authorRepository.findById(id)
                .orElseThrow(()->new ResourceNotFoundException("Author not found with id: " + id));
        author.setAuthorName(authorDto.getAuthorName());
        author=authorRepository.save(author);
        return authorMapper.toDto(author);
    }

    @Transactional
    @Override
    public void deleteAuthor(Long id) {
        Author author=authorRepository.findById(id)
                .orElseThrow(()->new ResourceNotFoundException("Author not found with id: " + id));
        authorRepository.delete(author);
    }

    @Transactional(readOnly = true)
    @Override
    public List<AuthorDto> findAllAuthors() {
        return authorRepository.findAll().stream()
                .map(authorMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<AuthorDto> searchAuthorByAuthorName(String authorName) {
        return authorRepository.findByAuthorNameContainingIgnoreCase(authorName).stream()
                .map(authorMapper::toDto)
                .collect(Collectors.toList());
    }
}
