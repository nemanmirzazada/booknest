package com.neman.booknest.Service.ServiceImpl;

import com.neman.booknest.Dto.UserDto;
import com.neman.booknest.Exceptions.ResourceAlreadyExistException;
import com.neman.booknest.Exceptions.ResourceNotFoundException;
import com.neman.booknest.Mapper.UserMapper;
import com.neman.booknest.Model.Role;
import com.neman.booknest.Model.User;
import com.neman.booknest.Repository.RoleRepository;
import com.neman.booknest.Repository.UserRepository;
import com.neman.booknest.Service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;
    @Override
    public UserDto registerUser(UserDto userDto) {
        if (userRepository.findByUsername(userDto.getUsername()).isPresent()){
            throw new ResourceAlreadyExistException("Username is already taken.");
        }

        User user=new User();
        user.setUsername(userDto.getUsername());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        Set<Role> roles=new HashSet<>();
        Role userRole=roleRepository.findByRoleName("USER");
        if (userRole==null){
            userRole=new Role();
            userRole.setRoleName("USER");
            roleRepository.save(userRole);
        }
        roles.add(userRole);
        user.setRoles(roles);
        return userMapper.toDto(userRepository.save(user));
    }

    @Transactional
    @Override
    public UserDto registerAdmin(UserDto userDto) {
        if (userRepository.findByUsername(userDto.getUsername()).isPresent()){
            throw new ResourceAlreadyExistException("Username is already exist");
        }
        User user=new User();
        user.setUsername(userDto.getUsername());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        Set<Role> roles=new HashSet<>();
        Role adminRole=roleRepository.findByRoleName("ADMIN");
        if (adminRole==null){
            adminRole=new Role();
            adminRole.setRoleName("ADMIN");
            roleRepository.save(adminRole);
        }
        roles.add(adminRole);
        user.setRoles(roles);
        return userMapper.toDto(userRepository.save(user));
    }


    @Override
    public UserDto findByUsername(String username) {
        return userMapper.toDto(userRepository.findByUsername(username).get());
    }

    @Transactional
    @Override
    public UserDto updateUser(Long id,UserDto userDto) {
        User existingUser = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User not found with id: " + id));

        existingUser.setUsername(userDto.getUsername());

        if (userDto.getPassword() != null && !userDto.getPassword().isEmpty()) {
            existingUser.setPassword(passwordEncoder.encode(userDto.getPassword()));
        }

        Set<Role> roles = new HashSet<>();
        for (String roleName : userDto.getRoleNames()) {
            Role role = roleRepository.findByRoleName(roleName);
            if (role == null) {
                role = new Role();
                role.setRoleName(roleName);
                roleRepository.save(role);
            }
            roles.add(role);
        }
        existingUser.setRoles(roles);

        User updatedUser = userRepository.save(existingUser);
        return userMapper.toDto(updatedUser);
    }
}
