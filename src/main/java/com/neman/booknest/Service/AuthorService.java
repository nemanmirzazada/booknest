package com.neman.booknest.Service;

import com.neman.booknest.Dto.AuthorDto;

import java.util.List;

public interface AuthorService {
    AuthorDto createAuthor(AuthorDto authorDto);
    AuthorDto getAuthorById(Long id);
    AuthorDto updateAuthor(Long id,AuthorDto authorDto);
    void deleteAuthor(Long id);
    List<AuthorDto> findAllAuthors();
    List<AuthorDto> searchAuthorByAuthorName(String authorName);
}
