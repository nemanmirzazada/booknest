package com.neman.booknest.Service;

import com.neman.booknest.Dto.UserDto;
import com.neman.booknest.Model.User;

public interface UserService {
    UserDto registerUser(UserDto userDto);
    UserDto registerAdmin(UserDto userDto);
    UserDto findByUsername(String username);
    UserDto updateUser(Long id,UserDto userDto);
}
