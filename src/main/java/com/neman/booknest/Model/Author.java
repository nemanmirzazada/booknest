package com.neman.booknest.Model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@ToString
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "author_id")
    Long authorId;
    @NotBlank(message = "authorName cannot be empty")
    String authorName;

    @ManyToMany(mappedBy = "authors",cascade = {CascadeType.PERSIST,CascadeType.MERGE},fetch = FetchType.LAZY)
    Set<Book> books=new HashSet<>();

    public void addBook(Book book){
        books.add(book);
        book.getAuthors().add(this);
    }

    public void removeBook(Book book){
        books.remove(book);
        book.getAuthors().remove(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Author)) return false;
        Author author = (Author) o;
        return authorId != null && authorId.equals(author.authorId);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
