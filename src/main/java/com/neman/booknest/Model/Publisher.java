package com.neman.booknest.Model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@ToString
public class Publisher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @NotBlank(message = "publisherName cannot be empty")
    String publisherName;

    @OneToMany(mappedBy = "publisher",cascade = CascadeType.ALL,orphanRemoval = true)
    Set<Book> books;
}
