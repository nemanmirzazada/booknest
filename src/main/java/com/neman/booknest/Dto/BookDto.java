package com.neman.booknest.Dto;


import com.neman.booknest.Model.Author;
import com.neman.booknest.Model.Publisher;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Set;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookDto {
    Long id;
    @NotBlank(message = "bookName cannot be empty")
    @Size(min = 3, max = 100, message = "bookName must be between 3 and 100 characters")
    String bookName;
    @Min(value = 0,message = "The price must be a non-negative number")
    BigDecimal bookPrice;

    @NotBlank(message = "Language cannot be empty")
    String language;

    @NotBlank(message = "ISBN cannot be empty")
    String ISBN;

    @Min(value = 0, message = "The price must be a non-negative number")
    Integer page;

    Set<Long> authorIds;
    Set<String> authorNames;

    Long publisherId;
    String publisherName;
    Publisher publisher;
    Set<Author> authors;
}
