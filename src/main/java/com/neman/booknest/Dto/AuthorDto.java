package com.neman.booknest.Dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthorDto {
    Long authorId;
    @NotBlank(message = "authorName cannot be empty")
    String authorName;
    Set <Long> bookIds;


}
