package com.neman.booknest.Dto;


import lombok.*;
import lombok.experimental.FieldDefaults;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PublisherDto {
    Long id;
    @NotBlank(message = "publisherName cannot be empty")
    String publisherName;
    Set<Long> bookIds;
    Set<String> bookNames;
}
