package com.neman.booknest.Dto;

import com.neman.booknest.Model.Role;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@ToString
public class UserDto {
  //  Long id;
    String username;
    String password;
    Set<String> roleNames;
}
