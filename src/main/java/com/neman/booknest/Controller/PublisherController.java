package com.neman.booknest.Controller;

import com.neman.booknest.Dto.PublisherDto;
import com.neman.booknest.Service.PublisherService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("publishers")
@RequiredArgsConstructor
public class PublisherController {
    private final PublisherService publisherService;

    @GetMapping
    public String getAllPublisher(Model model, @RequestParam(value = "search",required = false) String search){
        List<PublisherDto> publisherDtos;
        if (search!=null && !search.isEmpty()){
            publisherDtos=publisherService.searchPublisherByName(search);
        }else {
            publisherDtos=publisherService.findAllPublisher();
        }
        model.addAttribute("publishers",publisherDtos);
        return "publisherList";
    }

    @GetMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public String showCreateForum(Model model){
        model.addAttribute("publisher",new PublisherDto());
        return "publisherCreate";
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public String createPublisher(@ModelAttribute("publisher") PublisherDto publisherDto){
        publisherService.createPublisher(publisherDto);
        return "redirect:/publishers";
    }

    @GetMapping("/edit/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String showEditForum(@PathVariable Long id,Model model){
        PublisherDto publisherDto=publisherService.getPublisherById(id);
        model.addAttribute("publisher",publisherDto);
        return "publisherEdit";
    }

    @PostMapping("/edit/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String editPublisher(@PathVariable Long id,@ModelAttribute("publisher") PublisherDto publisherDto){
        publisherService.updatePublisher(id,publisherDto);
        return "redirect:/publishers";
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String deletePublisher(@PathVariable Long id){
        publisherService.deletePublisher(id);
        return "redirect:/publishers";
    }
}
