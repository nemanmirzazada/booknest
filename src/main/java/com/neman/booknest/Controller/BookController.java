package com.neman.booknest.Controller;

import com.neman.booknest.Dto.BookDto;
import com.neman.booknest.Model.Book;
import com.neman.booknest.Service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @GetMapping
    public String listBooks(Model model,@RequestParam(value = "search",required = false) String search){
        List<BookDto> books;
        if (search!=null && !search.isEmpty()){
            books=bookService.findBooksByTitle(search);
        }else {
            books=bookService.getAllBooks();
        }
        model.addAttribute("books",books);
        return "bookList";
    }

    @GetMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public String showCreateForum(Model model){
        model.addAttribute("book",new BookDto());
        return "BookCreate";
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public String createBook(@ModelAttribute("book") BookDto bookDto){
        bookService.createBook(bookDto);
        return "redirect:/books";
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String deleteBook(@PathVariable Long id){
        bookService.deleteBook(id);
        return "redirect:/books";
    }

    @PostMapping("edit/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String editBook(@PathVariable Long id,@ModelAttribute("book")BookDto bookDto){
        bookService.updateBook(id,bookDto);
        return "redirect:/books";
    }

    @GetMapping("edit/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String showEditForum(@PathVariable Long id,Model model){
        BookDto bookDto=bookService.getBookById(id);
        model.addAttribute("book",bookDto);
        return "BookEdit";
    }




}
