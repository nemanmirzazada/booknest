package com.neman.booknest.Controller;

import com.neman.booknest.Dto.UserDto;
import com.neman.booknest.Exceptions.ResourceAlreadyExistException;
import com.neman.booknest.Service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
public class HomeController {
    private final UserService userService;

    @GetMapping("/admin/index")
    public String adminIndex(Model model){
        model.addAttribute("message","Welcome to bookStore, Admin");
        return "index";
    }


    @GetMapping("/")
    public String home() {
        return "home";
    }

    @GetMapping("/login")
    public String login(@RequestParam(value = "error",required = false) String error,
                        @RequestParam(value = "logout",required = false) String logout,
                        Model model) {
        if (error!=null){
            model.addAttribute("error","Invalid username or password");
        }
        if (logout!=null){
            model.addAttribute("message","You have been logout successfully");
        }
        return "login";
    }

    @GetMapping("/register")
    public String showRegisterForum(Model model) {
        model.addAttribute("user", new UserDto());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@ModelAttribute("user") UserDto userDto,Model model){
        try {
            userService.registerUser(userDto);
        }catch (ResourceAlreadyExistException e){
            model.addAttribute("error",e.getMessage());
            return "register";
        }
        return "redirect:/login?success";
    }

    @GetMapping("/admin/register")
    public String showAdminRegistrationForm(Model model) {
        model.addAttribute("userDto", new UserDto());
        return "adminRegister";
    }

    @PostMapping("/admin/register")
    public String registerAdmin(UserDto userDto) {
        userService.registerAdmin(userDto);
        return "redirect:/login";
    }
}
