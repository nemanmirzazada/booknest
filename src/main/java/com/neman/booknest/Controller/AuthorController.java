package com.neman.booknest.Controller;

import com.neman.booknest.Dto.AuthorDto;
import com.neman.booknest.Model.Author;
import com.neman.booknest.Service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("authors")
@RequiredArgsConstructor
public class AuthorController {
    private final AuthorService authorService;

    @GetMapping
    public String getAllAuthors(Model model, @RequestParam(value = "search",required = false) String search){
        List<AuthorDto> authors;
        if (search!=null && !search.isEmpty()){
            authors=authorService.searchAuthorByAuthorName(search);
        }else {
            authors=authorService.findAllAuthors();
        }
        model.addAttribute("authors",authors);
        return "authorList";
    }

    @GetMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public String showCreateForum(Model model) {
        model.addAttribute("author", new AuthorDto());
        return "authorCreate";
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public String createAuthor(@ModelAttribute("author") AuthorDto authorDto){
        authorService.createAuthor(authorDto);
        return "redirect:/authors";
    }

    @GetMapping("/edit/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String showEditForum(@PathVariable Long id,Model model){
        AuthorDto authorDto=authorService.getAuthorById(id);
        model.addAttribute("author",authorDto);
        return "authorEdit";
    }

    @PostMapping("edit/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String editAuthor(@PathVariable Long id,@ModelAttribute("author") AuthorDto authorDto){
        authorService.updateAuthor(id, authorDto);
        return "redirect:/authors";
    }

    @GetMapping("delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String deleteAuthor(@PathVariable Long id){
        authorService.deleteAuthor(id);
        return "redirect:/authors";
    }


}
