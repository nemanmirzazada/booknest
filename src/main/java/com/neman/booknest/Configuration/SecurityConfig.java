package com.neman.booknest.Configuration;

import com.neman.booknest.Service.ServiceImpl.UserDetailsServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@RequiredArgsConstructor
public class SecurityConfig {
    private final UserDetailsServiceImpl userDetailsService;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                    .antMatchers("/", "/login", "/register", "/css/**", "/js/**").permitAll()
                    .antMatchers("/books", "/authors,","/publishers").hasAnyRole("USER","ADMIN")
                    .antMatchers("/books/**","/authors/**","publishers/**").hasRole("ADMIN")
                    .anyRequest().authenticated()
                .and()
                .formLogin()
                    .loginPage("/login")
                    .successHandler((request, response, authentication) -> {
                        String role=authentication.getAuthorities().iterator().next().getAuthority();
                        if (role.equals("ROLE_ADMIN")){
                            response.sendRedirect("/admin/index");
                        }else {
                            response.sendRedirect("/books");
                        }
                    })
                    .failureUrl("/login?error=true")
                    .permitAll()
                .and()
                .logout()
                    .logoutSuccessUrl("/login?logout=true")
                    .permitAll();

        return http.build();
    }




    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
