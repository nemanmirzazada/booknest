package com.neman.booknest.Repository;

import com.neman.booknest.Model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book,Long> {
    @Query("SELECT distinct b from Book b left join fetch b.authors left join fetch b.publisher")
    List<Book> findAllWithAuthorsAndPublisher();

    @Query("select b from Book b join fetch b.authors a where b.bookName like %:title%")
    List<Book> findByTitleContaining(@Param("title") String title);

    @Query("select b from Book b join fetch b.authors a where a.authorName like %:authorName%")
    List<Book> findByAuthorNameContaining(@Param("authorName") String authorName);

    @Query("select b from Book b join fetch b.publisher p where p.publisherName like %:publisherName%")
    List<Book> findByPublisherNameContaining(@Param("publisherName") String publisherName);
}
