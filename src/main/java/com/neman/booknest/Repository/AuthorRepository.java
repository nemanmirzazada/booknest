package com.neman.booknest.Repository;

import com.neman.booknest.Model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;


@Repository
public interface AuthorRepository extends JpaRepository<Author,Long> {
    boolean existsByAuthorName(String authorName);

    @Query("select a from Author a where lower(a.authorName) like lower(concat('%', :authorName, '%'))")
    List<Author> findByAuthorNameContainingIgnoreCase(@Param("authorName") String authorName);
}
