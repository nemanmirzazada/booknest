package com.neman.booknest.Repository;

import com.neman.booknest.Model.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher,Long> {
    boolean existsByPublisherName(String publisherName);

    @Query("select p from Publisher p where lower(p.publisherName) like lower(concat('%', :publisherName, '%') ) ")
    List<Publisher> findByPublisherNameContainingIgnoreCase(@Param("publisherName") String publisherName);

}
