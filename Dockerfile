FROM openjdk
RUN echo "Booknest-app"
COPY ./build/libs/Booknest-0.1.jar /app/
WORKDIR /app/


ENTRYPOINT ["java"]
CMD ["-jar", "/app/Booknest-0.1.jar"]
